/**
 * This file was auto-generated by Fern from our API Definition.
 */

import * as PlanqkServiceApi from "../../../index";

export type GetInterimResultsResponse =
    | PlanqkServiceApi.InterimResultResponse[]
    | PlanqkServiceApi.InterimResultResponse;
