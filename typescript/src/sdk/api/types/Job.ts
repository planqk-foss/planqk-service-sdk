/**
 * This file was auto-generated by Fern from our API Definition.
 */

import * as PlanqkServiceApi from "../index";

export interface Job {
    id?: string;
    status?: PlanqkServiceApi.JobStatus;
    createdAt?: string;
    startedAt?: string;
    endedAt?: string;
}
