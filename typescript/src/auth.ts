import {AccessToken, ClientCredentials} from 'simple-oauth2'

export const DEFAULT_TOKEN_ENDPOINT = 'https://gateway.platform.planqk.de'

export class PlanqkServiceAuth {
  private accessToken: AccessToken | undefined
  private readonly consumerKey: string
  private readonly consumerSecret: string
  private readonly tokenEndpoint: string

  constructor(consumerKey: string, consumerSecret: string, tokenEndpoint: string = DEFAULT_TOKEN_ENDPOINT) {
    this.consumerKey = consumerKey
    this.consumerSecret = consumerSecret
    this.tokenEndpoint = tokenEndpoint
  }

  async getAccessToken(): Promise<string> {
    if (!this.accessToken) {
      this.accessToken = await this.fetchAccessToken()
    }

    if (this.accessToken.expired()) {
      this.accessToken = await this.accessToken.refresh()
    }

    return this.accessToken.token.access_token as string
  }

  private fetchAccessToken(): Promise<AccessToken> {
    const client = new ClientCredentials({
      auth: {
        tokenHost: this.tokenEndpoint,
        tokenPath: '/token',
      },
      client: {
        id: this.consumerKey,
        secret: this.consumerSecret,
      },
    })
    return client.getToken({})
  }
}
