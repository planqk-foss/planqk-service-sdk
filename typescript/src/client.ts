import {DEFAULT_TOKEN_ENDPOINT, PlanqkServiceAuth} from './auth'
import {PlanqkServiceApiClient} from './sdk'
import {
  GetInterimResultsRequest,
  GetInterimResultsResponse,
  GetResultResponse,
  HealthCheckResponse,
  Job,
  JobStatus,
  NotFoundError,
  StartExecutionRequest,
} from './sdk/api'
import {ServiceApi} from './sdk/api/resources/serviceApi/client/Client'

export class PlanqkServiceClient {
  private _api: PlanqkServiceApiClient

  constructor(serviceEndpoint: string, consumerKey?: string, consumerSecret?: string, tokenEndpoint: string = DEFAULT_TOKEN_ENDPOINT) {
    if (consumerKey && consumerSecret) {
      const auth = new PlanqkServiceAuth(consumerKey, consumerSecret, tokenEndpoint)
      this._api = new PlanqkServiceApiClient({
        environment: () => serviceEndpoint,
        token: async () => auth.getAccessToken(),
      })
    } else {
      this._api = new PlanqkServiceApiClient({
        environment: () => serviceEndpoint,
        token: async () => Math.random().toString(36).slice(2),
      })
    }
  }

  public cancelExecution(jobId: string, requestOptions?: ServiceApi.RequestOptions): Promise<Job> {
    return this._api.serviceApi.cancelExecution(jobId, requestOptions)
  }

  public getInterimResults(jobId: string, request: GetInterimResultsRequest = {}, requestOptions?: ServiceApi.RequestOptions): Promise<GetInterimResultsResponse> {
    return this._api.serviceApi.getInterimResults(jobId, request, requestOptions)
  }

  public async getResult(jobId: string, requestOptions?: ServiceApi.RequestOptions): Promise<GetResultResponse> {
    await this.waitForFinalState(jobId)
    return this.doGetResultWithRetries(jobId, requestOptions)
  }

  public getStatus(jobId: string, requestOptions?: ServiceApi.RequestOptions): Promise<Job> {
    return this._api.serviceApi.getStatus(jobId, requestOptions)
  }

  public healthCheck(requestOptions?: ServiceApi.RequestOptions): Promise<HealthCheckResponse> {
    return this._api.statusApi.healthCheck(requestOptions)
  }

  public startExecution(request: StartExecutionRequest = {}, requestOptions?: ServiceApi.RequestOptions): Promise<Job> {
    return this._api.serviceApi.startExecution(request, requestOptions)
  }

  private async doGetResultWithRetries(jobId: string, requestOptions?: ServiceApi.RequestOptions, retries: number = 5, delay: number = 1000): Promise<GetResultResponse> {
    try {
      return await this._api.serviceApi.getResult(jobId, requestOptions)
    } catch (error) {
      if (error instanceof NotFoundError) {
        if (retries === 0) throw error
        // Wait for the specified delay
        await new Promise(resolve => {
          setTimeout(resolve, delay)
        })
        // Double the delay, but cap it at 16 seconds
        const newDelay = Math.min(delay * 2, 16_000)
        return this.doGetResultWithRetries(jobId, requestOptions, retries - 1, newDelay)
      }

      throw error
    }
  }

  private async doWaitForFinalState(jobId: string, timeout?: number, waitTime: number = 5, startTime: number = Date.now()): Promise<void> {
    const job = await this.getStatus(jobId)
    if (this.jobHasFinished(job)) return

    const elapsedTime = Date.now() - startTime
    if (timeout !== undefined && elapsedTime >= timeout * 1000) {
      throw new Error(`Timeout while waiting for job '${jobId}'.`)
    }

    await new Promise(resolve => {
      setTimeout(resolve, waitTime * 1000)
    })

    await this.doWaitForFinalState(jobId, timeout, waitTime, startTime)
  }

  private jobHasFinished(job: Job): boolean {
    return (job.status === JobStatus.Succeeded || job.status === JobStatus.Failed || job.status === JobStatus.Cancelled)
  }

  private async waitForFinalState(jobId: string, timeout?: number, wait: number = 5): Promise<void> {
    await this.doWaitForFinalState(jobId, timeout, wait)
  }
}
