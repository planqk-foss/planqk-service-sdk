export const complexInput = {
  data: {
    classes: [
      {
        attributes: [
          {
            cardinality: 'one',
            id: 'plane',
            name: 'Plane',
            type: 'plane',
          },
          {
            cardinality: 'one',
            id: 'pilot',
            name: 'Pilot',
            type: 'pilot',
          },
          {
            cardinality: 'many',
            id: 'blockedTimeslot',
            name: 'Blocked Timeslots',
            type: 'timeslot',
          },
        ],
        id: 'flight',
        name: 'Flight',
      },
      {
        attributes: [
          {
            cardinality: 'one',
            id: 'airline',
            name: 'Airline',
            type: 'airline',
          },
        ],
        id: 'plane',
        name: 'Plane',
      },
      {
        attributes: [],
        id: 'airline',
        name: 'Airline',
      },
      {
        attributes: [
          {
            cardinality: 'many',
            id: 'airlines',
            name: 'airlines',
            type: 'airline',
          },
          {
            cardinality: 'one',
            id: 'number',
            name: 'number',
            type: 'number',
          },
          {
            cardinality: 'one',
            id: 'terminal',
            name: 'terminal',
            type: 'terminal',
          },
        ],
        id: 'gate',
        name: 'Gate',
      },
      {
        attributes: [],
        id: 'timeslot',
        name: 'Timeslot',
      },
      {
        attributes: [
          {
            cardinality: 'one',
            id: 'name',
            name: 'Name',
            type: 'string',
          },
        ],
        id: 'pilot',
        name: 'Pilot',
      },
      {
        attributes: [],
        id: 'terminal',
        name: 'Terminal',
      },
      {
        attributes: [
          {
            cardinality: 'one',
            id: 'gate',
            name: 'Gate',
            type: 'gate',
          },
          {
            cardinality: 'one',
            id: 'timeslot',
            name: 'Timeslot',
            type: 'timeslot',
          },
        ],
        id: 'gateTimeslot',
        name: 'GateTimeslot',
      },
    ],
    constraints: [
      {
        description: '',
        id: 'DiagonalIncentive',
        name: 'DiagonalIncentive',
        type: 'DiagonalIncentive',
        value: {},
        weight: -0.25,
      },
      {
        description: '',
        id: 'constraint3',
        name: 'Every plane can only be on one gate per time',
        type: 'OnlyOneScopeElementInScope',
        value: {
          onlyOneOf: 'scopeX',
        },
        weight: 2,
      },
      {
        description: '',
        id: 'constraint4',
        name: 'Every gateTimeslot can only be on one timeslot',
        type: 'OnlyOneScopeElementInScope',
        value: {
          onlyOneOf: 'scopeY',
        },
        weight: 2,
      },
      {
        description: '',
        id: 'block-timeslots of flights',
        name: 'Timeslots of flights.blockedTimeslot should not be in gateTimeslot.timeslot',
        type: 'BlockAssignmentForScopeAttribute',
        value: {
          blockedScope: 'scopeY',
          forBlockingClass: 'timeslot',
          withBlockedAttributeIds: 'blockedTimeslot',
        },
        weight: 0.5,
      },
    ],
    objects: [
      {
        id: 'terminal1',
        name: 'Terminal 1',
        type: 'terminal',
        values: [],
      },
      {
        id: 'terminal2',
        name: 'Terminal 2',
        type: 'terminal',
        values: [],
      },
      {
        id: 'gate 1',
        name: 'Gate 1',
        type: 'gate',
        values: [
          {
            attributeId: 'airlines',
            value: [
              'emirates',
              'lufthansa',
            ],
          },
          {
            attributeId: 'number',
            value: '1',
          },
          {
            attributeId: 'terminal',
            value: 'terminal1',
          },
        ],
      },
      {
        id: 'gate2',
        name: 'Gate 2',
        type: 'gate',
        values: [
          {
            attributeId: 'airlines',
            value: [
              'emirates',
              'lufthansa',
            ],
          },
          {
            attributeId: 'number',
            value: '2',
          },
          {
            attributeId: 'terminal',
            value: 'terminal1',
          },
        ],
      },
      {
        id: 'gate3',
        name: 'Gate 3',
        type: 'gate',
        values: [
          {
            attributeId: 'airlines',
            value: [
              'emirates',
              'lufthansa',
            ],
          },
          {
            attributeId: 'number',
            value: '3',
          },
          {
            attributeId: 'terminal',
            value: 'terminal2',
          },
        ],
      },
      {
        id: 'lufthansa',
        name: 'Lufthansa',
        type: 'airline',
        values: [],
      },
      {
        id: 'emirates',
        name: 'Emirates',
        type: 'airline',
        values: [],
      },
      {
        id: 'timeslot1',
        name: '08:00',
        type: 'timeslot',
        values: [],
      },
      {
        id: 'timeslot2',
        name: '09:00',
        type: 'timeslot',
        values: [],
      },
      {
        id: 'timeslot3',
        name: '10:00',
        type: 'timeslot',
        values: [],
      },
      {
        id: 'pilot1',
        name: 'Pilot 1',
        type: 'pilot',
        values: [
          {
            attributeId: 'name',
            value: 'Felix',
          },
        ],
      },
      {
        id: 'pilot2',
        name: 'Pilot 2',
        type: 'pilot',
        values: [
          {
            attributeId: 'name',
            value: 'Benno',
          },
        ],
      },
      {
        id: 'plane1',
        name: 'Plane 1',
        type: 'plane',
        values: [
          {
            attributeId: 'airline',
            value: 'emirates',
          },
        ],
      },
      {
        id: 'plane2',
        name: 'Plane 2',
        type: 'plane',
        values: [
          {
            attributeId: 'airline',
            value: 'lufthansa',
          },
        ],
      },
      {
        id: 'XY001',
        name: 'Flight XY001',
        type: 'flight',
        values: [
          {
            attributeId: 'plane',
            value: 'plane1',
          },
          {
            attributeId: 'pilot',
            value: 'pilot1',
          },
          {
            attributeId: 'blockedTimeslot',
            value: [
              'timeslot1',
            ],
          },
        ],
      },
      {
        id: 'XY002',
        name: 'Flight XY002',
        type: 'flight',
        values: [
          {
            attributeId: 'plane',
            value: 'plane2',
          },
          {
            attributeId: 'pilot',
            value: 'pilot2',
          },
          {
            attributeId: 'blockedTimeslot',
            value: [
              'timeslot2',
            ],
          },
        ],
      },
      {
        id: 'XY003',
        name: 'Flight XY003',
        type: 'flight',
        values: [
          {
            attributeId: 'plane',
            value: 'plane2',
          },
          {
            attributeId: 'pilot',
            value: 'pilot2',
          },
          {
            attributeId: 'blockedTimeslot',
            value: [],
          },
        ],
      },
      {
        id: 'gate1timeslot1',
        name: 'gate1timeslot1',
        type: 'gateTimeslot',
        values: [
          {
            attributeId: 'gate',
            value: 'gate 1',
          },
          {
            attributeId: 'timeslot',
            value: 'timeslot1',
          },
        ],
      },
      {
        id: 'gate1timeslot2',
        name: 'gate1timeslot2',
        type: 'gateTimeslot',
        values: [
          {
            attributeId: 'gate',
            value: 'gate 1',
          },
          {
            attributeId: 'timeslot',
            value: 'timeslot2',
          },
        ],
      },
      {
        id: 'gate2timeslot1',
        name: 'gate2timeslot1',
        type: 'gateTimeslot',
        values: [
          {
            attributeId: 'gate',
            value: 'gate2',
          },
          {
            attributeId: 'timeslot',
            value: 'timeslot1',
          },
        ],
      },
      {
        id: 'gate2timeslot2',
        name: 'gate2timeslot2',
        type: 'gateTimeslot',
        values: [
          {
            attributeId: 'gate',
            value: 'gate2',
          },
          {
            attributeId: 'timeslot',
            value: 'timeslot2',
          },
        ],
      },
      {
        id: 'gate3timeslot2',
        name: 'gate3timeslot2',
        type: 'gateTimeslot',
        values: [
          {
            attributeId: 'gate',
            value: 'gate3',
          },
          {
            attributeId: 'timeslot',
            value: 'timeslot2',
          },
        ],
      },
      {
        id: 'gate3timeslot3',
        name: 'gate3timeslot3',
        type: 'gateTimeslot',
        values: [
          {
            attributeId: 'gate',
            value: 'gate3',
          },
          {
            attributeId: 'timeslot',
            value: 'timeslot3',
          },
        ],
      },
      {
        id: 'gate3timeslot1',
        name: 'gate3timeslot1',
        type: 'gateTimeslot',
        values: [
          {
            attributeId: 'gate',
            value: 'gate3',
          },
          {
            attributeId: 'timeslot',
            value: 'timeslot1',
          },
        ],
      },
    ],
    scopeX: 'flight',
    scopeY: 'gateTimeslot',
    solutions: [],
  },
  params: {},
}
