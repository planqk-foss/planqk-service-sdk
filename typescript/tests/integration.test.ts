import {PlanqkServiceAuth, PlanqkServiceClient} from '../src'
import {PlanqkServiceApiClient} from '../src/sdk'
import {JobStatus, StartExecutionRequest} from '../src/sdk/api'
import {complexInput} from './fixtures/complex-input'

const serviceEndpoint = process.env.SERVICE_ENDPOINT || 'http://localhost:8081'
const consumerKey = process.env.CONSUMER_KEY
const consumerSecret = process.env.CONSUMER_SECRET

test('should use client', async () => {
  const client = new PlanqkServiceClient(serviceEndpoint, consumerKey, consumerSecret)

  const health = await client.healthCheck()
  expect(health.status).toBe('Service is up and running')

  let job = await client.startExecution(complexInput)
  expect(job.id).toBeDefined()
  expect(job.status).toBe(JobStatus.Pending)

  job = await client.getStatus(job.id!)

  const result = await client.getResult(job.id!)
  expect(result).toBeDefined()

  job = await client.getStatus(job.id!)
  expect(job.status).toBe(JobStatus.Succeeded)
}, 5 * 60 * 1000)

test('should use raw client', async () => {
  let api: PlanqkServiceApiClient
  if (consumerKey && consumerSecret) {
    const auth = new PlanqkServiceAuth(consumerKey, consumerSecret)
    api = new PlanqkServiceApiClient({
      environment: () => serviceEndpoint,
      token: async () => auth.getAccessToken(),
    })
  } else {
    api = new PlanqkServiceApiClient({
      environment: () => serviceEndpoint,
      token: async () => Math.random().toString(36).slice(2),
    })
  }

  const health = await api.statusApi.healthCheck()
  expect(health.status).toBe('Service is up and running')

  const data = {input: {a: '1', b: '2'}}
  const params = {param1: 'value1', param2: 'value2'}

  let job = await api.serviceApi.startExecution({data, params})
  expect(job.id).toBeDefined()
  expect(job.status).toBe(JobStatus.Pending)

  job = await api.serviceApi.getStatus(job.id!)
  while (job.status !== JobStatus.Succeeded) {
    // eslint-disable-next-line no-await-in-loop
    job = await api.serviceApi.getStatus(job.id!)
  }

  expect(job.status).toBe(JobStatus.Succeeded)

  const result = await api.serviceApi.getResult(job.id!)
  expect(result).toBeDefined()
}, 5 * 60 * 1000)

test('should use client with data pool ref', async () => {
  const client = new PlanqkServiceClient(serviceEndpoint, consumerKey, consumerSecret)

  const health = await client.healthCheck()
  expect(health.status).toBe('Service is up and running')

  const request: StartExecutionRequest = {
    dataRef: {
      dataPoolId: 'e3859bb2-1959-4d3d-852b-62ee81b28a72',
      dataSourceDescriptorId: '1c8d341e-1c86-4e19-acae-2f84bcab55ca',
      fileId: 'f5251675-49c6-49f3-a3a6-de75b8335d76',
    },
    params: {},
  }

  let job = await client.startExecution(request)
  expect(job.id).toBeDefined()
  expect(job.status).toBe(JobStatus.Pending)

  job = await client.getStatus(job.id!)

  const result = await client.getResult(job.id!)
  expect(result).toBeDefined()

  job = await client.getStatus(job.id!)
  expect(job.status).toBe(JobStatus.Succeeded)
}, 5 * 60 * 1000)
