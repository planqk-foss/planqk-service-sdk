require('dotenv').config();

module.exports = {
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  preset: 'ts-jest',
  roots: ['<rootDir>'],
  testEnvironment: 'node',
  testRegex: './.*\\.(test|spec)?\\.ts$',
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
}
