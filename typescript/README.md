# PLANQK Service SDK

## Installation

The package can be installed using `npm`:

```bash
npm install @planqk/planqk-service-sdk
```

## Usage

```typescript
const serviceEndpoint = "..."
const consumerKey = "..."
const consumerSecret = "..."

const client = new PlanqkServiceClient(serviceEndpoint, consumerKey, consumerSecret)

// prepare your input data and parameters
const data = {"input": {"a": 1, "b": 2}}
const params = {"param1": "value1", "param2": "value2"}

// start the execution
let job = await client.startExecution({data, params})

// wait for the result
const result = await client.getResult(job.id!)
```

## Run Tests

```sh
SERVICE_ENDPOINT=<endpoint-url> CONSUMER_KEY=<consumer-key> CONSUMER_SECRET=<consumer-secret> npm run test
```
