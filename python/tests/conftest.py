from dotenv import load_dotenv


def pytest_configure():
    load_dotenv('.env')  # replace '.env' with the path to your .env file if it's not in the project root
