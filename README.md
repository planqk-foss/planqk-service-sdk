# planqk-service-sdk

This repository contains the SDK clients to execute PLANQK Managed Services.
The clients are generated from a very general OpenAPI specification using [fern](https://buildwithfern.com).
The generated clients are a baseline, and the idea is to adapt and extend them to our specific needs.

## Generate the SDK clients with `fern` as a baseline

> <https://buildwithfern.com/docs/getting-started>

```bash
npm install -g fern-api
fern upgrade
fern generate --local
```

## How to update the Python base client?

After generating the SDK clients with `fern`, the Python base client needs to be copied manually to the `python` folder.

Copy the content of `./generated/python` to `./python/planqk/service/sdk`.
Make sure you overwrite any existing files.
It is recommended to remove the old files first.

Next, check, e.g., by executing `pytest`, if the changes are compatible with our wrapper.

## How to update the TypeScript base client?

After generating the SDK clients with `fern`, the Typescript base client needs to be copied manually to the `typescript` folder.

Copy the content of `./generated/typescript` to `./typescript/src/sdk`.
Make sure you overwrite any existing files.
It is recommended to remove the old files first.

Next, check, e.g., by executing `npm test`, if the changes are compatible with our wrapper.

## License

Apache-2.0 | Copyright 2023-2024 Kipu Quantum GmbH
